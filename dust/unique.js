/**
 * Created by serkandemirel on 15/06/2017.
 */
/**
 * Extract unique objects
 * @param {Array} data
 * @param {String} key to filter
 * @return {Number} Array with unique items
 */
module.exports = function(dust) {
    dust.helpers.unique = function (chunk, context, bodies, params) {
        var data = params.data;
        let key = params.key
        var uniqueOnes = [];
        data.reduce(function(curent, next){
            (curent["key"] === next["key"]) ? uniqueOnes.push(curent) : uniqueOnes.push(next);
            return next;
        });
        return uniqueOnes;
    }
};